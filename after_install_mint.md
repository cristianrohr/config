# Actualizar repos y upgrade
sudo apt-get update
sudo apt-get upgrade

# Instalar tilix
sudo add-apt-repository ppa:webupd8team/terminix
sudo apt-get update
sudo apt-get install tilix

# Instalar git
sudo apt-get install git-all

# Compresion
sudo apt-get install rar unace p7zip p7zip-full p7zip-rar unrar lzip lhasa arj sharutils mpack lzma lzop cabextract

# Productividad
sudo apt-get install zsh python3-pip vim-gnome i3 curl libncurses-dev python-dev build-essential cmake libfreetype6-dev feh libcurl4-gnutls-dev pandoc libxml2-dev

# Java Runtime Environment (JRE)**
sudo apt-get install default-jre

# Fuentes restrictivas
sudo apt-get install ttf-mscorefonts-installer


# ONT
**To add the Oxford Nanopore apt-get repository, run the command below on a terminal window:**

sudo apt-get update
sudo apt-get install wget
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt trusty-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list

**Install MinKNOW using the command:**

sudo apt-get update
sudo apt-get install libgtk2.0 libxss1 libxtst6 libnss3 libgconf-2-4 libasound2 minknow-map

Pero falla por lo siguiente:
Los siguientes paquetes tienen dependencias incumplidas:
 minknow-map : Depende: libicu52 (>= 52.1) pero no es instalable
E: No se pudieron corregir los problemas, usted ha retenido paquetes rotos.

Lo que hice fue bajar el .deb que se encuentra en ~/ONT/requerimientos y lo instale de ahí, luego sequi.

**The application directory is found in /opt/ONT/MinKNOW**

**Location of the reads folder:**
The reads folder is in /var/lib/MinKNOW/data/reads with 3 sub-folders:
./pass
./skip
./fail



# Instalar temas & iconos

**Flat-Remix** https://github.com/daniruiz/Flat-Remix
cd /tmp
git clone https://github.com/daniruiz/Flat-Remix
mkdir -p ~/.icons
mv "Flat-Remix/Flat Remix" ~/.icons

**Oranchelo** http://www.omgubuntu.co.uk/2016/08/oranchelo-flat-icon-theme-linux-desktop
sudo add-apt-repository ppa:oranchelo/oranchelo-icon-theme
sudo apt-get update && sudo apt-get install oranchelo-icon-theme


# Mejorar las fuentes
**https://mintguide.org/themes/421-improve-the-appearance-of-fonts-on-linux-mint.html**
sudo add-apt-repository ppa:no1wantdthisname/ppa
sudo apt-get update
sudo apt-get install fontconfig-infinality

sudo bash /etc/fonts/infinality/infctl.sh setstyle
**Choose linux**

sudo gedit /etc/profile.d/infinality-settings.sh
**and find the line "USE_STYLE="DEFAULT". Change it accordingly DEFAULT on UBUNTU. Save, exit.**


sudo mv /etc/profile.d/infinality-settings.sh /etc/infinality-settings.sh
sudo chmod a+rx /etc/infinality-settings.sh

sudo apt-get install fonts-cantarell lmodern ttf-aenigma ttf-georgewilliams ttf-bitstream-vera ttf-sjfonts ttf-tuffy tv-fonts


# Preparar SD para Linux
https://sites.google.com/site/easylinuxtipsproject/ssd
